import { Hono, Context } from "hono";
import { env } from "hono/adapter";

// 連携先REST APIへプロキシする関数
const proxy = async (c: Context) => {
  const {
    DISTRIBUTED_AUTH_TOKEN,
    TARGET_HOST,
    TARGET_HOST_VERY_SECRET_AUTH_TOKEN,
  } = env<{
    /** 自前の認証トークン (`Bearer XXXXXX`) */
    DISTRIBUTED_AUTH_TOKEN: string;
    /** もともとの宛先（プロキシ先） */
    TARGET_HOST: string;
    /** もともとの宛先の認証トークン (`Bearer YYYYYY`) */
    TARGET_HOST_VERY_SECRET_AUTH_TOKEN: string;
  }>(c);

  // 自前の認証トークンを提示しているかチェック
  console.log("req auth", c.req.header("Authorization"));
  if (DISTRIBUTED_AUTH_TOKEN !== c.req.header("Authorization")) {
    c.status(403);
    return c.json({ message: "Authorization token mismatched" });
  }

  // プロキシ用のリクエストを作成
  const targetHost = new URL(TARGET_HOST);
  const newUrl = Object.assign(new URL(c.req.url), {
    protocol: targetHost.protocol,
    hostname: targetHost.hostname,
    port: targetHost.port,
  });
  const newHeaders = new Headers(c.req.raw.headers);
  newHeaders.set("Authorization", TARGET_HOST_VERY_SECRET_AUTH_TOKEN);
  const req = new Request(newUrl.toString(), {
    method: c.req.method,
    headers: newHeaders,
    body: ["GET", "HEAD"].includes(c.req.method)
      ? undefined
      : await c.req.blob(),
  });

  // 連携先のAPIサーバにリクエスト発行 / 連携元にそのままレスポンス
  console.log(`Proxying to ${c.req.method} ${newUrl}...`);
  const res = await fetch(req);
  console.log(`  => ${res.status} ${res.statusText}`);
  return res;
};

// プロキシが必要なAPIエンドポイントのみプロキシを適用
export default new Hono()
  .post("/api/v1/proxied_api", proxy)
  .all("/api/v1/another_proxied_api", proxy);
