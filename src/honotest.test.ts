import { describe, expect, test } from "vitest";
import { Hono } from "hono";

describe("hono fetch", async () => {
  // Honoのサーバを作成＆fetch(本体)を取り出す
  const app = new Hono().get("/hello", async (c) => new Response("World"));
  const { fetch } = app;

  // fetch を用いてFetch APIと同じ感覚でリクエストを投げてみる
  test("/hello", async () => {
    const res = await fetch(new Request("http://example.com/hello"));
    expect(await res.text()).toBe("World");
  });

  test("/should_not_be_found", async () => {
    const res = await fetch(
      new Request("http://example.com/should_not_be_found")
    );
    expect(res.status).toBe(404);
  });
});
