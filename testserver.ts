import { Hono } from "hono";

const app = new Hono().all("/*", async (c) => {
  console.log({ req: c.req.raw, body: await c.req.text() });
  c.status(200);
  return c.json({ message: "Hello" });
});

export default {
  port: 18888,
  fetch: app.fetch,
};
