## Hono によるプロキシの実装例

```bash
$ bun install

# テストサーバを動かす
$ bun run testserver

# プロキシサーバ(本体)を動かす
$ bun run dev

# リクエストを投げる
$ bash testpost.sh
$ bun testpost.js
$ node testpost.js
```
